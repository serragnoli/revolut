# Tech Stack
* Java 8
* Maven 3
* DropWizard
* Guice
* Assert-J
* Mockito

# Package Structure
* rest -> Delivery mechanism
* actions -> Tells what the system does 
* domain -> Tells what the system is about
* model -> Contains all models of the domain
* infrastructure -> Contains all technology-dependent classes that support the domain

# Running
* mvn clean verify
* java -jar target/MoneyTransfer-1.0.0-SNAPSHOT.jar server money-transfer.yml
* There will be two accounts created by defaul (123 and 321) both with 100 balance
+ Make transfer: POST http://localhost:8070/v1/accounts/transfer
     * Body: 
          {
              "sender": {
                  "accountNumber": "123",
                  "sendingAmount": "10"
              },
              "recipient": {
                  "account": "321"
              }
          }
* Check account balance: GET http://localhost:8070/v1/accounts/{accountNumber}/balance