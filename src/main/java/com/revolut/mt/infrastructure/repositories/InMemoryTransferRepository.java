package com.revolut.mt.infrastructure.repositories;

import com.revolut.mt.domain.transfer.TransferRepository;
import com.revolut.mt.model.Account;

import java.math.BigDecimal;

public class InMemoryTransferRepository implements TransferRepository {

    @Override
    public BigDecimal debit(final String accountNumber, final BigDecimal amount) {
        final Account debitableAccount = DataStore.DATA_STORE.get(accountNumber);
        final Account newBalanceAccount = debitableAccount.debit(amount);

        DataStore.DATA_STORE.put(accountNumber, newBalanceAccount);

        return amount;
    }

    @Override
    public void credit(final String accountNumber, final BigDecimal amount) {
        final Account recipientAccount = DataStore.DATA_STORE.get(accountNumber);

        DataStore.DATA_STORE.put(accountNumber, recipientAccount.credit(amount));
    }
}
