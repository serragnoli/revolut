package com.revolut.mt.infrastructure.repositories;

import com.revolut.mt.domain.account.exceptions.AccountAlreadyExistsException;
import com.revolut.mt.domain.account.exceptions.AccountCannotBeFoundException;
import com.revolut.mt.domain.account.AccountRepository;
import com.revolut.mt.model.Account;
import com.revolut.mt.model.Recipient;

import java.math.BigDecimal;

public class InMemoryAccountRepository implements AccountRepository {

    @Override
    public void add(final Account account) {
        if(DataStore.DATA_STORE.containsKey(account.getAccountNumber())) {
            throw new AccountAlreadyExistsException();
        }

        DataStore.DATA_STORE.put(account.getAccountNumber(), account);
    }

    @Override
    public BigDecimal fetchBalanceOf(final String account) {
        if(!DataStore.DATA_STORE.containsKey(account)) {
            throw new AccountCannotBeFoundException();
        }

        return DataStore.DATA_STORE.get(account).getBalance();
    }

    @Override
    public void clear() {
        DataStore.DATA_STORE.clear();
    }

    @Override
    public Account fetchAccountOf(final Recipient recipient) {
        return DataStore.DATA_STORE.get(recipient.getAccount());
    }

    @Override
    public Account fetch(final String accountNumber) {
        return DataStore.DATA_STORE.get(accountNumber);
    }
}
