package com.revolut.mt.infrastructure.config;

public final class Paths {

    public static final String ACCOUNTS = "/v1/accounts";

    public static final String TRANSFER = "/transfer";
    public static final String ACCOUNTS_TRANSFER = ACCOUNTS + TRANSFER;

    public static final String CHECK_BALANCE_PATH_PARAM = "accountNumber";
    public static final String CHECK_BALANCE = "/{" + CHECK_BALANCE_PATH_PARAM + "}/balance";
    public static final String VERIFY_BALANCE = ACCOUNTS + CHECK_BALANCE;

    private Paths() {
    }
}
