package com.revolut.mt.infrastructure.config;

import com.google.inject.AbstractModule;
import com.revolut.mt.domain.account.AccountRepository;
import com.revolut.mt.infrastructure.repositories.InMemoryTransferRepository;
import com.revolut.mt.domain.transfer.TransferRepository;
import com.revolut.mt.infrastructure.repositories.InMemoryAccountRepository;

public class MoneyTransferModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(AccountRepository.class).to(InMemoryAccountRepository.class);
        bind(TransferRepository.class).to(InMemoryTransferRepository.class);
    }
}
