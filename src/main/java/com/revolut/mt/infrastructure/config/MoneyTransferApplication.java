package com.revolut.mt.infrastructure.config;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.revolut.mt.actions.MakeTransfer;
import com.revolut.mt.infrastructure.repositories.DataStore;
import com.revolut.mt.model.Account;
import com.revolut.mt.rest.resources.TransferResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

import java.math.BigDecimal;

public class MoneyTransferApplication extends Application<MoneyTransferConfiguration> {

    public static void main(String[] args) throws Exception {
        DataStore.DATA_STORE.put("123", new Account("123", BigDecimal.valueOf(100)));
        DataStore.DATA_STORE.put("321", new Account("321", BigDecimal.valueOf(100)));

        new MoneyTransferApplication().run(args);
    }

    @Override
    public void run(MoneyTransferConfiguration configuration, Environment environment) throws Exception {
        final Injector injector = Guice.createInjector(new MoneyTransferModule());

        environment.jersey().register(injector.getInstance(TransferResource.class));
        environment.jersey().register(injector.getInstance(MakeTransfer.class));
    }
}
