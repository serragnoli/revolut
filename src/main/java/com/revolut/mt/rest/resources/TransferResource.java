package com.revolut.mt.rest.resources;

import com.revolut.mt.actions.FetchAccountBalance;
import com.revolut.mt.actions.MakeTransfer;
import com.revolut.mt.model.AccountTransferRequest;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.math.BigDecimal;

import static com.revolut.mt.infrastructure.config.Paths.ACCOUNTS;
import static com.revolut.mt.infrastructure.config.Paths.CHECK_BALANCE;
import static com.revolut.mt.infrastructure.config.Paths.CHECK_BALANCE_PATH_PARAM;
import static com.revolut.mt.infrastructure.config.Paths.TRANSFER;

@Path(ACCOUNTS)
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TransferResource {

    private final MakeTransfer makeTransfer;
    private final FetchAccountBalance fetchAccountBalance;

    @Inject
    public TransferResource(final MakeTransfer makeTransfer, final FetchAccountBalance fetchAccountBalance) {
        this.makeTransfer = makeTransfer;
        this.fetchAccountBalance = fetchAccountBalance;
    }

    @POST
    @Path(TRANSFER)
    public Response transfer(final AccountTransferRequest transferRequest) {
        makeTransfer.transfer(transferRequest);

        return Response.noContent().build();
    }

    @GET
    @Path(CHECK_BALANCE)
    public Response checkBalanceFor(@PathParam(CHECK_BALANCE_PATH_PARAM) String accountNumber) {
        final BigDecimal balance = fetchAccountBalance.fetchBalanceFor(accountNumber);

        return Response.ok(balance).build();
    }
}
