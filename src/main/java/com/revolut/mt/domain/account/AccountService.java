package com.revolut.mt.domain.account;

import com.revolut.mt.model.Account;
import com.revolut.mt.model.Recipient;
import com.revolut.mt.model.Sender;

import javax.inject.Inject;
import java.math.BigDecimal;

public class AccountService {

    private final AccountRepository repository;
    private final AccountValidator validator;

    @Inject
    public AccountService(final AccountRepository repository, final AccountValidator validator) {
        this.repository = repository;
        this.validator = validator;
    }

    public void ensureBalanceOf(final Sender sender) {
        final BigDecimal currentBalance = repository.fetchBalanceOf(sender.getAccountNumber());

        validator.validate(currentBalance, sender.getSendingAmount());
    }

    public void verifyAccountOf(final Recipient recipient) {
        final Account recipientAccount = repository.fetchAccountOf(recipient);

        validator.validate(recipientAccount);
    }
}
