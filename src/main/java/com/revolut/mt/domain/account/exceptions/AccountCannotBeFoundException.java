package com.revolut.mt.domain.account.exceptions;

public class AccountCannotBeFoundException extends RuntimeException {
}
