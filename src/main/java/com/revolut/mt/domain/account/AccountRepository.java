package com.revolut.mt.domain.account;

import com.revolut.mt.model.Account;
import com.revolut.mt.model.Recipient;

import java.math.BigDecimal;

public interface AccountRepository {
    void add(Account account);

    BigDecimal fetchBalanceOf(final String account);

    Account fetchAccountOf(Recipient recipient);

    Account fetch(String accountNumber);

    void clear();
}
