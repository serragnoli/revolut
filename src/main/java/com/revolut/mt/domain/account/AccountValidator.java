package com.revolut.mt.domain.account;

import com.revolut.mt.domain.account.exceptions.AccountCannotBeFoundException;
import com.revolut.mt.domain.account.exceptions.InsufficientBalanceException;
import com.revolut.mt.model.Account;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;

public class AccountValidator {

    private static final int NEGATIVE = -1;

    public void validate(final BigDecimal balance, final BigDecimal transfer) {

        if (transfer.compareTo(ZERO) == NEGATIVE) {
            throw new IllegalArgumentException();
        }

        if (balance.compareTo(transfer) == NEGATIVE) {
            throw new InsufficientBalanceException();
        }
    }

    public void validate(final Account account) {
        if(null == account) {
            throw new AccountCannotBeFoundException();
        }
    }
}
