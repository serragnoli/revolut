package com.revolut.mt.domain.transfer;

import java.math.BigDecimal;

public interface TransferRepository {

    BigDecimal debit(String accountNumber, final BigDecimal amount);

    void credit(String accountNumber, BigDecimal amount);
}
