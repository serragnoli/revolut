package com.revolut.mt.domain.transfer;

import com.revolut.mt.model.Recipient;
import com.revolut.mt.model.Sender;

import javax.inject.Inject;
import java.math.BigDecimal;

public class TransferService {

    private final TransferRepository repository;

    @Inject
    public TransferService(final TransferRepository repository) {
        this.repository = repository;
    }

    public void fulfilTransfer(final Sender from, final Recipient to) {
        final BigDecimal debitedFromSender = repository.debit(from.getAccountNumber(), from.getSendingAmount());

        repository.credit(to.getAccount(), debitedFromSender);
    }
}
