package com.revolut.mt.actions;

import com.revolut.mt.domain.account.AccountRepository;
import com.revolut.mt.domain.account.AccountValidator;
import com.revolut.mt.model.Account;

import javax.inject.Inject;
import java.math.BigDecimal;

public class FetchAccountBalance {

    private final AccountRepository repository;
    private final AccountValidator validator;

    @Inject
    public FetchAccountBalance(final AccountRepository repository, final AccountValidator validator) {
        this.repository = repository;
        this.validator = validator;
    }

    public BigDecimal fetchBalanceFor(final String accountNumber) {
        final Account fetchedAccount = repository.fetch(accountNumber);

        validator.validate(fetchedAccount);

        return fetchedAccount.getBalance();
    }
}
