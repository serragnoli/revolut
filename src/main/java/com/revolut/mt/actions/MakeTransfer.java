package com.revolut.mt.actions;

import com.revolut.mt.domain.account.AccountService;
import com.revolut.mt.domain.transfer.TransferService;
import com.revolut.mt.model.AccountTransferRequest;

import javax.inject.Inject;

public class MakeTransfer {

    private final AccountService accountService;
    private final TransferService transferService;

    @Inject
    public MakeTransfer(final AccountService accountService, final TransferService transferService) {
        this.accountService = accountService;
        this.transferService = transferService;
    }

    public void transfer(final AccountTransferRequest transferRequest) {
        accountService.ensureBalanceOf(transferRequest.getSender());

        accountService.verifyAccountOf(transferRequest.getRecipient());

        transferService.fulfilTransfer(transferRequest.getSender(), transferRequest.getRecipient());
    }
}
