package com.revolut.mt.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Account {

    private String accountNumber;
    private BigDecimal balance;

    public Account credit(final BigDecimal amount) {
        return new Account(this.accountNumber, balance.add(amount));
    }

    public Account debit(final BigDecimal amount) {
        return new Account(this.accountNumber, balance.subtract(amount));
    }
}
