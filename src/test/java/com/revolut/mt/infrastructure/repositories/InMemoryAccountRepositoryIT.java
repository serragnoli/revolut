package com.revolut.mt.infrastructure.repositories;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.revolut.mt.domain.account.exceptions.AccountAlreadyExistsException;
import com.revolut.mt.domain.account.exceptions.AccountCannotBeFoundException;
import com.revolut.mt.domain.account.AccountRepository;
import com.revolut.mt.model.Account;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;

import static com.revolut.mt.domain.account.TestCommons.*;
import static com.revolut.mt.domain.account.TestCommons.RECIPIENT_ACCOUNT_NUMBER;
import static com.revolut.mt.domain.account.TestCommons.SENDER_ACCOUNT_NUMBER;
import static com.revolut.mt.rest.AccountFactory.accountWith;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.rules.ExpectedException.none;

public class InMemoryAccountRepositoryIT {

    @Rule
    public ExpectedException thrown = none();

    private AccountRepository accountRepository;

    @Before
    public void setUp() {
        final Injector injector = Guice.createInjector();

        accountRepository = injector.getInstance(InMemoryAccountRepository.class);
    }

    @Test
    public void should_fetch_account_balance() {
        accountRepository.add(accountWith(SENDER_ACCOUNT_NUMBER, TEN));

        final BigDecimal balance = accountRepository.fetchBalanceOf(SENDER_ACCOUNT_NUMBER);

        assertThat(balance).isEqualTo(TEN);
    }

    @Test(expected = AccountCannotBeFoundException.class)
    public void should_throw_exception_when_account_inexistent() {
        accountRepository.fetchBalanceOf("INEXISTENT");
    }

    @Test
    public void should_throw_exception_if_account_already_exists() {
        accountRepository.add(accountWith(SENDER_ACCOUNT_NUMBER, TEN));

        thrown.expect(AccountAlreadyExistsException.class);
        accountRepository.add(accountWith(SENDER_ACCOUNT_NUMBER, ONE));
    }

    @Test
    public void should_retrieve_account() {
        accountRepository.add(accountWith(RECIPIENT_ACCOUNT_NUMBER, ONE));

        final Account fetchedAccount = accountRepository.fetchAccountOf(RECIPIENT);

        assertThat(fetchedAccount).isEqualTo(accountWith(RECIPIENT_ACCOUNT_NUMBER, ONE));
    }

    @Test
    public void should_retrieve_null_when_account_inexistent() {
        final Account fetchedAccount = accountRepository.fetchAccountOf(RECIPIENT);

        assertThat(fetchedAccount).isNull();
    }

    @Test
    public void should_retrieve_account_given_account_number() {
        accountRepository.add(accountWith(RECIPIENT_ACCOUNT_NUMBER, ONE));

        final Account fetchedAccount = accountRepository.fetch(RECIPIENT_ACCOUNT_NUMBER);

        assertThat(fetchedAccount).isEqualTo(accountWith(RECIPIENT_ACCOUNT_NUMBER, ONE));
    }

    @Test
    public void should_retrieve_null_when_account_number_inexistent() {
        final Account fetchedAccount = accountRepository.fetch(RECIPIENT_ACCOUNT_NUMBER);

        assertThat(fetchedAccount).isNull();
    }

    @After
    public void tearDown() {
        accountRepository.clear();
    }
}