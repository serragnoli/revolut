package com.revolut.mt.domain.transfer;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.revolut.mt.domain.account.AccountRepository;
import com.revolut.mt.infrastructure.repositories.InMemoryAccountRepository;
import com.revolut.mt.infrastructure.repositories.InMemoryTransferRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.revolut.mt.domain.account.TestCommons.RECIPIENT_ACCOUNT_NUMBER;
import static com.revolut.mt.domain.account.TestCommons.SENDER_ACCOUNT_NUMBER;
import static com.revolut.mt.rest.AccountFactory.accountWith;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.valueOf;
import static org.assertj.core.api.Assertions.assertThat;

public class TransferRepositoryIT {

    private AccountRepository accountRepository;

    private TransferRepository repository;

    @Before
    public void setUp() {
        final Injector injector = Guice.createInjector();

        accountRepository = injector.getInstance(InMemoryAccountRepository.class);

        repository = injector.getInstance(InMemoryTransferRepository.class);
    }

    @Test
    public void should_debit_from_sender_account() {
        accountRepository.add(accountWith(SENDER_ACCOUNT_NUMBER, TEN));

        repository.debit(SENDER_ACCOUNT_NUMBER, ONE);

        assertThat(accountRepository.fetchBalanceOf(SENDER_ACCOUNT_NUMBER)).isEqualTo(valueOf(9));
    }

    @Test
    public void should_credit_to_recipient_account() {
        accountRepository.add(accountWith(RECIPIENT_ACCOUNT_NUMBER, TEN));

        repository.credit(RECIPIENT_ACCOUNT_NUMBER, ONE);

        assertThat(accountRepository.fetchBalanceOf(RECIPIENT_ACCOUNT_NUMBER)).isEqualTo(valueOf(11));
    }

    @After
    public void tearDown() {
        accountRepository.clear();
    }
}