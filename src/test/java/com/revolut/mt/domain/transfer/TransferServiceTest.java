package com.revolut.mt.domain.transfer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static com.revolut.mt.domain.account.TestCommons.RECIPIENT;
import static com.revolut.mt.domain.account.TestCommons.SENDER_GBP_10;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TransferServiceTest {

    @Mock
    private TransferRepository repository;

    @InjectMocks
    private TransferService service;

    @Test
    public void should_invoke_repository() {
        service.fulfilTransfer(SENDER_GBP_10, RECIPIENT);

        verify(repository).debit(anyString(), any(BigDecimal.class));
    }

    @Test
    public void should_invoke_repository_to_persist_recipient_new_balance() {
        service.fulfilTransfer(SENDER_GBP_10, RECIPIENT);

        verify(repository).credit(anyString(),any(BigDecimal.class));
    }

    @Test
    public void credit_should_never_be_before_debit() {
        service.fulfilTransfer(SENDER_GBP_10, RECIPIENT);

        InOrder inOrder = inOrder(repository);
        inOrder.verify(repository).debit(anyString(), any(BigDecimal.class));
        inOrder.verify(repository).credit(anyString(), any(BigDecimal.class));
    }
}