package com.revolut.mt.domain.account;

import com.revolut.mt.model.Account;
import com.revolut.mt.model.Recipient;
import com.revolut.mt.model.Sender;

import static java.math.BigDecimal.TEN;

public class TestCommons {

    public static final String SENDER_ACCOUNT_NUMBER = "123";

    public static final String RECIPIENT_ACCOUNT_NUMBER = "321";

    public static final Account NO_ACCOUNT = null;

    public static final Sender SENDER_GBP_10 = Sender.builder().accountNumber(SENDER_ACCOUNT_NUMBER).sendingAmount(TEN).build();
    public static final Recipient RECIPIENT = Recipient.builder().account(RECIPIENT_ACCOUNT_NUMBER).build();
}
