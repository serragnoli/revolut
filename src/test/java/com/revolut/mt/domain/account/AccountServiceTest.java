package com.revolut.mt.domain.account;

import com.revolut.mt.model.Account;
import com.revolut.mt.model.Recipient;
import com.revolut.mt.model.Sender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static com.revolut.mt.domain.account.TestCommons.RECIPIENT_ACCOUNT_NUMBER;
import static com.revolut.mt.domain.account.TestCommons.SENDER_ACCOUNT_NUMBER;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @Mock
    private AccountRepository repository;

    @Mock
    private AccountValidator validator;

    @InjectMocks
    private AccountService accountService;

    @Test
    public void should_invoke_repository() {
        final Sender sender = Sender.builder().accountNumber(SENDER_ACCOUNT_NUMBER).sendingAmount(ONE).build();

        accountService.ensureBalanceOf(sender);

        verify(repository).fetchBalanceOf(SENDER_ACCOUNT_NUMBER);
    }

    @Test
    public void should_invoke_repository_to_verify_account() {
        accountService.verifyAccountOf(any(Recipient.class));

        verify(repository).fetchAccountOf(any(Recipient.class));
    }

    @Test
    public void should_invoke_validator_for_recipient_account() {
        final Recipient recipient = Recipient.builder().account(RECIPIENT_ACCOUNT_NUMBER).build();

        accountService.verifyAccountOf(recipient);

        verify(validator).validate(any(Account.class));
    }

    @Test
    public void should_invoke_validator() {
        final Sender sender = Sender.builder().accountNumber(SENDER_ACCOUNT_NUMBER).sendingAmount(TEN).build();

        accountService.ensureBalanceOf(sender);

        verify(validator).validate(any(BigDecimal.class), any(BigDecimal.class));
    }
}