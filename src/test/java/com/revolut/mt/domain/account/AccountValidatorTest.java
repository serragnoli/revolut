package com.revolut.mt.domain.account;

import com.revolut.mt.domain.account.exceptions.AccountCannotBeFoundException;
import com.revolut.mt.domain.account.exceptions.InsufficientBalanceException;
import com.revolut.mt.rest.AccountFactory;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.valueOf;
import static org.junit.rules.ExpectedException.none;

@RunWith(MockitoJUnitRunner.class)
public class AccountValidatorTest {

    @Rule
    public ExpectedException thrown = none();

    @InjectMocks
    private AccountValidator validate;

    @Test(expected = InsufficientBalanceException.class)
    public void should_throw_exception_when_balance_insufficient() {
        validate.validate(ONE, TEN);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception_when_transfer_is_negative() {
        validate.validate(TEN, valueOf(-1));
    }

    @Test
    public void should_validate_balance_equal_transfer() {
        validate.validate(TEN, TEN);
    }

    @Test
    public void should_validate_balance_greater_than_transfer() {
        validate.validate(TEN, ONE);
    }

    @Test(expected = AccountCannotBeFoundException.class)
    public void should_thrown_exception_when_account_null() {
        validate.validate(TestCommons.NO_ACCOUNT);
    }

    @Test
    public void should_validate_existent_account() {
        validate.validate(AccountFactory.accountWith(TEN));
    }
}