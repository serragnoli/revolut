package com.revolut.mt.rest;

import com.revolut.mt.model.Account;

import java.math.BigDecimal;

import static com.revolut.mt.domain.account.TestCommons.SENDER_ACCOUNT_NUMBER;

public class AccountFactory {
    public static Account accountWith(final BigDecimal balance) {
        return new Account(SENDER_ACCOUNT_NUMBER, balance);
    }

    public static Account accountWith(final String accountNumber, final BigDecimal initialBalance) {
        return new Account(accountNumber, initialBalance);
    }
}
