package com.revolut.mt.rest;

import com.revolut.mt.model.AccountTransferRequest;
import com.revolut.mt.model.Recipient;
import com.revolut.mt.model.Sender;

import static com.revolut.mt.domain.account.TestCommons.RECIPIENT_ACCOUNT_NUMBER;
import static com.revolut.mt.domain.account.TestCommons.SENDER_ACCOUNT_NUMBER;
import static java.math.BigDecimal.valueOf;

public class AccountTransferRequestFactory {

    public static AccountTransferRequest transfer51FromBalanceOf100() {
        final Sender sender = Sender.builder().sendingAmount(valueOf(51)).accountNumber(SENDER_ACCOUNT_NUMBER).build();
        final Recipient recipient = Recipient.builder().account(RECIPIENT_ACCOUNT_NUMBER).build();

        return new AccountTransferRequest(sender, recipient);
    }
}
