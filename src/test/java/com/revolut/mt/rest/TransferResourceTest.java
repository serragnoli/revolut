package com.revolut.mt.rest;

import com.revolut.mt.actions.FetchAccountBalance;
import com.revolut.mt.actions.MakeTransfer;
import com.revolut.mt.model.AccountTransferRequest;
import com.revolut.mt.rest.resources.TransferResource;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.Rule;
import org.junit.Test;

import static com.revolut.mt.domain.account.TestCommons.SENDER_ACCOUNT_NUMBER;
import static com.revolut.mt.infrastructure.config.Paths.ACCOUNTS_TRANSFER;
import static com.revolut.mt.infrastructure.config.Paths.CHECK_BALANCE_PATH_PARAM;
import static com.revolut.mt.infrastructure.config.Paths.VERIFY_BALANCE;
import static com.revolut.mt.rest.AccountTransferRequestFactory.transfer51FromBalanceOf100;
import static javax.ws.rs.client.Entity.json;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TransferResourceTest {

    private MakeTransfer makeTransfer = mock(MakeTransfer.class);

    private FetchAccountBalance fetchAccountBalance = mock(FetchAccountBalance.class);

    @Rule
    public final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new TransferResource(makeTransfer, fetchAccountBalance))
            .build();

    @Test
    public void should_invoke_domain_entry_point() {
        resources.client().target(ACCOUNTS_TRANSFER).request()
                .post(json(transfer51FromBalanceOf100()));

        verify(makeTransfer).transfer(any(AccountTransferRequest.class));
    }

    @Test
    public void should_invoke_account_domain_entry_point() {
        resources.client().target(normalise(VERIFY_BALANCE)).request().get();

        verify(fetchAccountBalance).fetchBalanceFor(anyString());
    }

    private String normalise(final String verifyBalance) {
        return verifyBalance.replaceAll("\\{" + CHECK_BALANCE_PATH_PARAM + "\\}", SENDER_ACCOUNT_NUMBER);
    }
}