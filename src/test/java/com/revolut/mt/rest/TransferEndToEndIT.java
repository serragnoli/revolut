package com.revolut.mt.rest;


import com.revolut.mt.infrastructure.config.MoneyTransferApplication;
import com.revolut.mt.infrastructure.config.MoneyTransferConfiguration;
import com.revolut.mt.infrastructure.config.MoneyTransferModule;
import com.revolut.mt.infrastructure.repositories.InMemoryAccountRepository;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import java.math.BigDecimal;

import static com.google.inject.Guice.createInjector;
import static com.revolut.mt.domain.account.TestCommons.RECIPIENT_ACCOUNT_NUMBER;
import static com.revolut.mt.domain.account.TestCommons.SENDER_ACCOUNT_NUMBER;
import static com.revolut.mt.infrastructure.config.Paths.ACCOUNTS_TRANSFER;
import static com.revolut.mt.rest.AccountFactory.accountWith;
import static com.revolut.mt.rest.AccountTransferRequestFactory.transfer51FromBalanceOf100;
import static java.lang.String.format;
import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;
import static javax.ws.rs.client.Entity.json;
import static org.assertj.core.api.Assertions.assertThat;

public class TransferEndToEndIT {

    @ClassRule
    public static final DropwizardAppRule<MoneyTransferConfiguration> APP_RULE =
            new DropwizardAppRule<>(MoneyTransferApplication.class, ResourceHelpers.resourceFilePath("money-transfer-qa.yml"));

    private static final String BASE_URL = "http://localhost:%s";
    private static final InMemoryAccountRepository REPOSITORY = createInjector(new MoneyTransferModule()).getInstance(InMemoryAccountRepository.class);

    private final Client client = new JerseyClientBuilder(APP_RULE.getEnvironment()).build("Test Client");

    private final String accountTransfersUrl = BASE_URL + ACCOUNTS_TRANSFER;
    private final Invocation.Builder makeTransferRequest = client.target(format(accountTransfersUrl, APP_RULE.getLocalPort())).request();

    private final String verifySenderBalanceUrl = BASE_URL + "/v1/accounts/" + SENDER_ACCOUNT_NUMBER + "/balance";
    private final Invocation.Builder checkSenderBalanceRequest = client.target(format(verifySenderBalanceUrl, APP_RULE.getLocalPort())).request();

    private final String verifyRecipientBalanceUrl = BASE_URL + "/v1/accounts/" + RECIPIENT_ACCOUNT_NUMBER + "/balance";
    private final Invocation.Builder checkRecipientBalanceRequest = client.target(format(verifyRecipientBalanceUrl, APP_RULE.getLocalPort())).request();

    @Before
    public void setUp() {
        REPOSITORY.add(accountWith(SENDER_ACCOUNT_NUMBER, valueOf(100)));
        REPOSITORY.add(accountWith(RECIPIENT_ACCOUNT_NUMBER, ZERO));
    }

    @Test
    public void should_transfer_51() {
        makeTransferRequest.post(json(transfer51FromBalanceOf100()));

        final BigDecimal senderBalance = checkSenderBalanceRequest.get().readEntity(BigDecimal.class);
        assertThat(senderBalance).isEqualTo(valueOf(49));

        final BigDecimal recipientBalance = checkRecipientBalanceRequest.get().readEntity(BigDecimal.class);
        assertThat(recipientBalance).isEqualTo(valueOf(51));
    }

    @After
    public void tearDown() {
        REPOSITORY.clear();
    }
}
