package com.revolut.mt.actions;

import com.revolut.mt.domain.account.AccountRepository;
import com.revolut.mt.domain.account.AccountValidator;
import com.revolut.mt.model.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.revolut.mt.rest.AccountFactory.accountWith;
import static java.math.BigDecimal.TEN;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FetchAccountBalanceTest {

    @Mock
    private AccountValidator validator;

    @Mock
    private AccountRepository repository;

    @InjectMocks
    private FetchAccountBalance fetchAccountBalance;

    @Test
    public void should_invoke_repository() {
        when(repository.fetch(anyString())).thenReturn(accountWith(TEN));

        fetchAccountBalance.fetchBalanceFor(anyString());

        verify(repository).fetch(anyString());
    }

    @Test
    public void should_invoke_validator() {
        when(repository.fetch(anyString())).thenReturn(accountWith(TEN));

        fetchAccountBalance.fetchBalanceFor(anyString());

        verify(validator).validate(any(Account.class));
    }
}