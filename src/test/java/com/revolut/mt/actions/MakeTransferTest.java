package com.revolut.mt.actions;

import com.revolut.mt.domain.account.AccountService;
import com.revolut.mt.domain.transfer.TransferService;
import com.revolut.mt.model.Recipient;
import com.revolut.mt.model.Sender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.revolut.mt.rest.AccountTransferRequestFactory.transfer51FromBalanceOf100;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MakeTransferTest {

    @Mock
    private AccountService accountService;

    @Mock
    private TransferService transferService;

    @InjectMocks
    private MakeTransfer makeTransfer;

    @Test
    public void should_invoke_account_service() {
        makeTransfer.transfer(transfer51FromBalanceOf100());

        verify(accountService).ensureBalanceOf(any(Sender.class));
    }

    @Test
    public void should_invoke_account_service_to_verify_recipient_account() {
        makeTransfer.transfer(transfer51FromBalanceOf100());

        verify(accountService).verifyAccountOf(any(Recipient.class));
    }

    @Test
    public void should_invoke_transfer_service() {
        makeTransfer.transfer(transfer51FromBalanceOf100());

        verify(transferService).fulfilTransfer(any(Sender.class), any(Recipient.class));
    }

    @Test
    public void should_invoke_account_service_before_transfer_service() {
        makeTransfer.transfer(transfer51FromBalanceOf100());

        InOrder inOrder = inOrder(accountService, transferService);
        inOrder.verify(accountService).ensureBalanceOf(any(Sender.class));
        inOrder.verify(transferService).fulfilTransfer(any(Sender.class), any(Recipient.class));
    }
}