package com.revolut.mt.model;

import com.revolut.mt.rest.AccountFactory;
import org.junit.Test;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.valueOf;
import static org.assertj.core.api.Assertions.assertThat;

public class AccountTest {

    @Test
    public void should_debit_account() {
        final Account debitableAccount = AccountFactory.accountWith(TEN);

        final Account newBalanceAccount = debitableAccount.debit(ONE);

        assertThat(newBalanceAccount.getBalance()).isEqualTo(valueOf(9));
    }

    @Test
    public void should_return_new_instance_of_account_after_debit() {
        final Account debitableAccount = AccountFactory.accountWith(TEN);

        final Account newBalanceAccount = debitableAccount.debit(ONE);

        assertThat(newBalanceAccount).isNotSameAs(debitableAccount);
    }

    @Test
    public void should_credit_account() {
        final Account creditableAccount = AccountFactory.accountWith(TEN);

        final Account newBalanceAccount = creditableAccount.credit(ONE);

        assertThat(newBalanceAccount.getBalance()).isEqualTo(valueOf(11));
    }

    @Test
    public void should_return_new_instance_of_account_after_credit() {
        final Account creditableAccount = AccountFactory.accountWith(TEN);

        final Account newBalanceAccount = creditableAccount.credit(ONE);

        assertThat(newBalanceAccount).isNotSameAs(creditableAccount);
    }
}